public class IcfesConvert {
    public static void main(String[] args) {

        // initialize variables new exams and formula

        double nLC, nM, nCN, nSC, nI, form;

        // initialize and set variables old exams

        double aL = 62.21;
        double aM = 58.38;
        double aCS = 69.34;
        double aFi = 57.9;
        double aB = 55.09;
        double aQ = 56.56;
        double aF = 60.76;
        double aCP = 5.2;
        double aI = 59.67;

        //Re-formulate new exam test - no use aCP in new exam case

        nLC = aL;
        nM = aM;
        nCN = (aB + aQ + aF) / 3;
        nSC = (aCS + aFi) / 2;
        nI = aI;

        // final formula

        form = (((nLC*3)+(nM*3)+(nCN*3)+(nSC*3)+(nI*1))/13)*5;

        // output: final score

        System.out.println("Puntaje Global: " + form);

    }
}
