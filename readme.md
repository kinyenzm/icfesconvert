<center><h1>
ICFES 11 | SABER 11 [es-CO]
</h1></center>

<center><h3>
Conversión de 2014- a 2014+
</h3></center>
  
![icfes-logo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Icfes_Colombia_logo.svg/1280px-Icfes_Colombia_logo.svg.png)
###  Propuesta

Usar los resultados de años anteriores al 2014 a la prueba de estado colombiano [ icfes ] para obtener la nueva medición `Puntaje Global` que es presente a partir de esta fecha anteriormente mencionada.

### Clasificicación

Listando significado de las variables a usar en esté archivo ejecutable en para terminal 💡

<center><h4>
Variables
</h4></center>

|Nombre Prueba|Despue/Antes 2014|Variable|Tipo de dato|
|:-|:-:|:-:|:-:|
|LENGUAJE |Antes|`aL`|double
|MATEMÁTICA|Antes|`aM`|double
|CIENCIAS SOCIALES|Antes|`aCS`|double
|FILOSOFÍA|Antes|`aFI`|double
|BIOLOGÍA|Antes|`aB`|double
|QUÍMICA|Antes|`aQ`|double
|FÍSICA|Antes|`aQ`|double
|COMPONENTE FLEXIBLE|Antes|`aCP`|double
|INGLÉS|Antes|`aI`|double

> Se omite el uso de COMPONENTE FLEXIBLE `aCP`, este no se incluye a un equivalente en las actuales pruebas de estado.

---

|Nombre Prueba|Despues/Antes 2014|Variable|Tipo de dato|
|:-|:-:|:-:|:-:|
|LITERATURA CRÍTICA|Antes|`nLC`|double
|MATEMÁTICAS|Antes|`nM`|double
|SOCIALES Y CIUDADANÍA|Antes|`nCN`|double
|SOCIALES Y CIUDADANÍA|Antes|`nSC`|double
|INGLÉS|Antes|`nI`|double
  
 --- 
<center><h4>
PROPUESTA DE ADAPTACIÓN
</h4></center>
  

#### Fórmula para examen actual

$$
\left(
\frac
{(LC\times3 )+(M\times3 )+(SC\times3 )+(CN\times3)+(I\times1)}
{13\ }\right)
\times5
$$

> ref: [sitio](https://como-saber.com/colombia/como-saber-mi-puntaje-icfes/)
 
---
#### Ajustando variables de pruebas anteriores a las de ahora:

|Variable anterior||Nuevo valor de variable|
|-:|-|:-|
`nLC`|=|`aL`|
`nM` |=|`aM`|
`nCN`|=|(`aB` + `aQ` + `aF`) / 3
`nSC`|=|(`aCS` + `aFi`) / 2
`nI`|=|`aI`

---  

#### Ajustando la formula inicial:

$$
\left(
\frac
{(nLC\times\ 3\ \ )+\ (nM\ \times3\ )+\ (nSC\times3\ )+\ (nCN\times3\ )+\ (nI\ \times1\ )}
{13\ }\right)
\times5
$$

## Contribución
         
👋⁣💻🔨📝🎨

Todo es bienvenida! 

## Licencia
MIT