aL = 62.21
aM = 58.38
aCS = 69.34
aFi = 57.9
aB = 55.09
aQ = 56.56
aF = 60.76
aCP = 5.2
aI = 59.67

nLC = aL
nM = aM
nCN = (aB + aQ + aF) / 3
nSC = (aCS + aFi) / 2
nI = aI


def run():
    form = (((nLC * 3) + (nM * 3) + (nCN * 3) + (nSC * 3) + (nI * 1)) / 13) * 5
    print(f'Puntaje Global: {form}')


if __name__ == "__main__":
    run()
